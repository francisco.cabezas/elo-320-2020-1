#include <stdio.h> 
#include <string.h>

int main () {
    char str1[12] = "Hello", str2[12] = "World", str3[12]; 
    int len;

    printf ("Size of str1: %lu\n", sizeof (str1)); 
    printf ("Length of str1: %lu\n", strlen(str1));

    strcpy (str3, str1); /* copy str1 into str3 */
    printf ("strcpy( str3, str1) : %s\n", str3 );
    strcat (str1, str2); /* concatenates str1 and str2 */ 
    printf ("strcat( str1, str2): %s\n", str1 );

    len = strlen (str1); /* total length of str1 after concatenation */ 
    printf ("strlen(str1) : %d\n", len );

    return 0;
} 
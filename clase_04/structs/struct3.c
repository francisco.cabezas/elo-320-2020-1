#include<stdio.h>
#include<string.h>

typedef struct estudiante {
    int id;
    int edad;
    char nombre[100];
    int nota;
} student;

void imprimeAlumno(student*);

int main() {
    student alumno1;

    alumno1.id = 12345;
    alumno1.edad = 20;
    strcpy(alumno1.nombre, "Patricio Cabrera");
    alumno1.nota = 80;

    student alumno2 = {21325, 21, "Fernanda Molina", 90};

    printf("Información Alumno 1:\n");
    imprimeAlumno(&alumno1);
    printf("\n");
    imprimeAlumno(&alumno2);
}

void imprimeAlumno(student *alumno) {
    printf("ID: %d\n", alumno->id);
    printf("Edad: %d\n", alumno->edad);
    printf("Nombre: %s\n", alumno->nombre);
    printf("Nota: %d\n", (*alumno).nota);
}
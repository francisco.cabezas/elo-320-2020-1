#include <stdio.h> 
#include <string.h>

typedef struct estudiante {
    int id;
    int edad;
    int nota;
    char* nombre;
} student;

void imprimeAlumno(student *);

int main() {
    student* alumno1; // pointer to structure of type
    
    alumno1->id = 12345; 
    alumno1->edad = 24;
    alumno1->nota = 80;
    alumno1->nombre = "Patricio Cabrera";

    imprimeAlumno(alumno1);

    return 0;
} 

void imprimeAlumno(student *alumno) {
    printf("ID: %d\n", alumno->id);
    printf("Edad: %d\n", alumno->edad);
    printf("Nombre: %s\n", alumno->nombre);
    printf("Nota: %d\n", (*alumno).nota);
}
#include<stdio.h>
#include<string.h>

struct estudiante {
    int id;
    int edad;
    char nombre[100];
    int nota;
};

void imprimeAlumno(struct estudiante*);

int main() {
    struct estudiante alumno1;

    alumno1.id = 12345;
    alumno1.edad = 20;
    strcpy(alumno1.nombre, "Patricio Cabrera");
    alumno1.nota = 80;

    struct estudiante alumno2 = {21325, 21, "Fernanda Molina", 90};

    printf("Información Alumno 1:\n");
    imprimeAlumno(&alumno1);
    printf("\n");
    imprimeAlumno(&alumno2);
}

void imprimeAlumno(struct estudiante *alumno) {
    printf("ID: %d\n", alumno->id);
    printf("Edad: %d\n", alumno->edad);
    printf("Nombre: %s\n", alumno->nombre);
    printf("Nota: %d\n", (*alumno).nota);
}
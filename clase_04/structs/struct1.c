#include<stdio.h>
#include<string.h>

struct estudiante {
    int id;
    int edad;
    char nombre[100];
    int nota;
};

int main() {
    struct estudiante alumno1;

    alumno1.id = 12345;
    alumno1.edad = 20;
    strcpy(alumno1.nombre, "Patricio Cabrera");
    alumno1.nota = 80;

    struct estudiante alumno2 = {21325, 21, "Fernanda Molina", 90};

    printf("Información Alumno 1:\n");
    printf("ID: %d\n", alumno1.id);
    printf("Edad: %d\n", alumno1.edad);
    printf("Nombre: %s\n", alumno1.nombre);
    printf("Nota: %d\n", alumno1.nota);
    printf("\n");
    printf("Información Alumno 2:\n");
    printf("ID: %d\n", alumno2.id);
    printf("Edad: %d\n", alumno2.edad);
    printf("Nombre: %s\n", alumno2.nombre);
    printf("Nota: %d\n", alumno2.nota);
}
#include<stdio.h>
#include<stdlib.h>

typedef struct node {
    int data;
    struct node *next;
    struct node *previous;
} node;

node* crearNodo(int);
void insertFirst(int, node**);
void insertLast(int, node**);
int isEmpty();
void printAll(node*);

int main() {
    node* head = NULL;

    insertFirst(10, &head);
    printf("Head data: %d\n", head->data);

    insertLast(20, &head);
    printf("Head data: %d\n", head->data);

    insertFirst(30, &head);
    printf("Head data: %d\n", head->data);

    insertLast(40, &head);
    printf("Head data: %d\n", head->data);

    printAll(head);

    return 0;
}

void insertFirst(int data, node** head) {
    node* newNode = crearNodo(data);
    if(isEmpty(*head)) {
        *head = newNode;
        return;
    }
    (*head)->previous = newNode;
    newNode->next = *head;
    *head = newNode;
}

void insertLast(int data, node** head) {
    node* current = *head;
    node* newNode = crearNodo(data);
    if(isEmpty(*head)) {
        *head = newNode;
        return;
    }
    while(current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
    current->previous = current;
}

node* crearNodo(int data) {
    node* newNode = (node*) malloc(sizeof(node));
    if(newNode == NULL) {
        printf("Error al crear nuevo nodo\n");
        exit(0);
    }
    newNode->data = data;
    newNode->next = NULL;
    newNode->previous = NULL;

    return newNode;
}

int isEmpty(node* head) {
    return head == NULL;
}

void printAll(node* head) {
    node* current = head;
    int i = 1;
    while(current != NULL) {
        printf("Node %d data: %d\n", i, current->data);
        if(current->previous == NULL) {
            printf("Node %d previous is NULL\n", i);
        } else {
            printf("Node %d previous node data: %d\n", i, current->previous->data);
        }
        if(current->next == NULL) {
            printf("Node %d next is NULL\n", i);
        } else {
            printf("Node %d next node data: %d\n", i, current->next->data);
        }
        current = current->next;
        i++;
    }
}
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "./stack.h"

int top = -1;
int stack[MAX_SIZE];

int main() {

    printf("Valor de TOP antes de ejecutar push(): %d\n", top);

    push(10);
    push(20);
    push(30);

    printf("Valor de TOP después de ejecutar 3 push(): %d\n", top);

    for(int i = 0; i < MAX_SIZE; i++) {
        printf("stack[%d] = %d\n", i, stack[i]);
    }

    pop();
    pop();

    printf("Valor de TOP después de ejecutar 2 pop(): %d\n", top);

    for(int i = 0; i < MAX_SIZE; i++) {
        printf("stack[%d] = %d\n", i, stack[i]);
    }

    return 0;
}

int pop() {
    int data = 0;
    if(!isEmpty()) {
        data = stack[top];
        stack[top] = 0;
        top = top - 1;
    } else {
        printf("Stack is empty\n");
    }
    return data;
}

void push(int data) {
    if(!isFull()) {
        top = top + 1;
        stack[top] = data;
    } else {
        printf("Stack Overflow\n");
        exit(-1);
    }
}

bool isFull() {
    if(getStackSize() == MAX_SIZE - 1) {
        return true;
    } else {
        return false;
    }
}

bool isEmpty() {
    if(top == -1) {
        return true;
    } else {
        return false;
    }
}

int getStackSize() {
    return top;
}
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "./stack.h"

int top = -1;
node* element = NULL;

int main() {

    printf("Valor de TOP antes de ejecutar push(): %d\n", top);

    push(10);
    push(20);
    push(30);

    printf("Valor de TOP después de ejecutar 3 push(): %d\n", top);

    printAll(element);

    pop();
    pop();

    printf("Valor de TOP después de ejecutar 2 pop(): %d\n", top);

    printAll(element);

    pop();

    printf("Valor de TOP después de ejecutar 1 vez más pop(): %d\n", top);

    printAll(element);

    return 0;
}

node* crearNodo(int data, node* next) {
    node* newNode = (node*) malloc(sizeof(node));
    if(newNode == NULL) {
        printf("Error al crear nuevo nodo\n");
        exit(-1);
    }
    newNode->data = data;
    newNode->next = next;

    return newNode;
}

// Agrega el nodo al final de la lista, si no hay nodos previos, devuelve el nuevo nodo
node* append(int data, node* head) {
    if(head != NULL) {
        node *cursor = head;
        while (cursor->next != NULL) {
            cursor = cursor->next; 
        }
        node* new_node = crearNodo(data, NULL);
        cursor->next = new_node;
    } else {
        head = crearNodo(data, NULL);
    }
    return head;
}

void printAll(node* head) {
    node* current = head;
    int i = 1;
    while(current != NULL) {
        printf("Node %d data: %d\n", i++, current->data);
        current = current->next;
    }
}

int removeLastNode(node** head) {
    int data = 0;
    if(head != NULL) {
        node *cursor = *head;
        if(cursor->next != NULL) {
            while (cursor->next->next != NULL) {
                cursor = cursor->next; 
            }
            data = cursor->next->data;
            cursor->next = NULL;
            free(cursor->next);
            return data;
        } else {
            data = (*head)->data;
            *head = NULL;
            free(*head);
            return data;
        }
    }
    return data;
}

int pop() {
    int data = 0;
    if(!isEmpty()) {
        data = removeLastNode(&element);
        top = top - 1;
    } else {
        printf("Stack is empty\n");
    }
    return data;
}

void push(int data) {
    if(!isFull()) {
        top = top + 1;
        element = append(data, element);
    } else {
        printf("Stack Overflow\n");
        exit(-1);
    }
}

bool isFull() {
    if(getStackSize() == MAX_SIZE - 1) {
        return true;
    } else {
        return false;
    }
}

bool isEmpty() {
    if(top == -1) {
        return true;
    } else {
        return false;
    }
}

int getStackSize() {
    return top;
}
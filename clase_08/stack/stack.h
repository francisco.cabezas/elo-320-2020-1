#define MAX_SIZE 10

typedef struct node {
    int data;
    struct node *next;
} node;

node* crearNodo(int, node*);
node* append(int, node*);
int removeLastNode(node**);
void printAll(node* head);

void push(int);
int pop();
bool isFull();
bool isEmpty();
int getStackSize();
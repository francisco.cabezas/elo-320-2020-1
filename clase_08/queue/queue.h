#define MAX_SIZE 6

bool enqueue(int);
int dequeue();
bool isFull();
bool isEmpty();
void printQueue();
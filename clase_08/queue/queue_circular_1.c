#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "./queue.h"

int queue[MAX_SIZE];
int front = -1;
int rear = -1;

int main() {

    printf("-------------------------\n");

    enqueue(1);
    enqueue(2);
    enqueue(3);
    enqueue(4);
    enqueue(5);
    enqueue(6);

    printQueue();

    printf("-------------------------\n");

    dequeue();

    printQueue();

    printf("-------------------------\n");

    dequeue();
    dequeue();

    printQueue();

    printf("-------------------------\n");

    dequeue();
    dequeue();

    printQueue();

    return 0;
}

bool enqueue(int data) {
    if(isFull()) {
        return false;
    } else if(isEmpty()) {
        front = 0;
    }
    rear = (rear + 1) % MAX_SIZE;
    queue[rear] = data;
    return true;
}

int dequeue() {
    if(isEmpty()) { // Vacío
        return -1;
    }
    int data = queue[front];
    queue[front] = 0;
    if(front == rear) {
        front = -1;
        rear = -1;
    } else {
        front = (front + 1) % MAX_SIZE;
    }
    return data;
}

bool isFull() {
    if(rear == MAX_SIZE - 1) {
        return true;
    } else {
        return false;
    }
}

bool isEmpty() {
    if(front == -1) {
        return true;
    } else {
        return false;
    }
}

void printQueue() {
    for(int i = 0; i < MAX_SIZE; i++) {
        printf("queue[%d] = %d\n", i, queue[i]);
    }
}
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "./queue.h"

int queue[MAX_SIZE];
int front = -1;
int rear = -1;

int main() {

    printf("-------------------------\n");

    enqueue(1);
    enqueue(2);
    enqueue(3);
    enqueue(4);
    enqueue(5);
    enqueue(6);

    printQueue();

    printf("-------------------------\n");

    dequeue();

    printQueue();

    printf("-------------------------\n");

    dequeue();
    dequeue();

    printQueue();

    printf("-------------------------\n");

    dequeue();
    dequeue();

    printQueue();

    return 0;
}

bool enqueue(int data) {
    if(rear == MAX_SIZE - 1) {
        return false;
    } else if(front == -1) {
        front = 0;
    }
    rear = rear + 1;
    queue[rear] = data;
    return true;
}

int dequeue() {
    if(front == -1) { // Vacío
        return -1;
    }
    int data = queue[front];
    queue[front] = 0;
    front = front + 1;
    if(front > rear) {
        front = -1;
        rear = -1;
    }
    return data;
}

void printQueue() {
    for(int i = 0; i < MAX_SIZE; i++) {
        printf("queue[%d] = %d\n", i, queue[i]);
    }
}
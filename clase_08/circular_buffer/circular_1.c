#include<stdio.h>
#include<stdlib.h>

typedef struct node {
    int data;
    struct node *next;
} node;

node* crearNodo(int, node*);
void insertFirst(int, node**);
void insertLast(int, node**);
void printAll(node*);

int main() {
    node* head = NULL;

    insertFirst(10, &head);
    printf("Head data: %d\n", head->data);

    insertFirst(20, &head);
    printf("Head data: %d\n", head->data);

    insertFirst(30, &head);
    printf("Head data: %d\n", head->data);

    insertFirst(40, &head);
    printf("Head data: %d\n", head->data);

    insertLast(100, &head);
    printf("Head data: %d\n", head->data);

    insertLast(200, &head);
    printf("Head data: %d\n", head->data);

    insertLast(500, &head);
    printf("Head data: %d\n", head->data);

    printAll(head);

    return 0;
}

void insertFirst(int data, node** head) {
    *head = crearNodo(data, *head);
}

node* crearNodo(int data, node* head) {
    node* newNode = (node*) malloc(sizeof(node));
    if(newNode == NULL) {
        printf("Error al crear nuevo nodo\n");
        exit(0);
    }
    newNode->data = data;

    if(head == NULL) {
        head = newNode;
        newNode->next = head;
    } else {
        newNode->next = head->next;
        head->next = newNode;
    }

    return newNode;
}

void insertLast(int data, node** head) {
    node* newNode = (node*) malloc(sizeof(node));
    if(newNode == NULL) {
        printf("Error al crear nuevo nodo\n");
        exit(0);
    }
    newNode->data = data;

    if((*head) == NULL) {
        *head = newNode;
        newNode->next = *head;
    } else {
        newNode->next = (*head)->next;
        (*head)->next = newNode;
        *head = newNode;
    }

    return;
}

void printAll(node* head) {
    node* current = head;
    int firstViewed = head->data;
    int i = 1;
    do {
        printf("Current node %d data: %d\n", i++, current->data);
        printf("Next node data: %d\n", current->next->data);
        current = current->next;
    } while(current->data != firstViewed);
}
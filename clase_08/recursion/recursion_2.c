#include <stdio.h>

typedef unsigned long long int xlarge_int;
typedef unsigned int large_int;

xlarge_int factorial(large_int);

int main () {
    int i = 4;
    printf("Factorial de %d es %llu\n", i, factorial(i));
    return 0;
}

xlarge_int factorial(large_int i) {
    if(i <= 1) {
        return 1;
    }
    return i * factorial(i - 1);
}
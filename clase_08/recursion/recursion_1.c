#include <stdio.h>

void printNum(int);

int main () {
    printNum(1);
    return 0;
}

void printNum(int number) {
    if(number < 5) {
        printNum(number + 1);
    }
    printf("%d\n", number);
}
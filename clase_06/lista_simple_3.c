#include<stdio.h>
#include<stdlib.h>

typedef struct node {
    int data;
    struct node *next;
} node;

node* crearNodo(int, node*);
void insertFirst(int, node**);
void printAll(node*);

int main() {
    node* head = NULL;

    insertFirst(10, &head);
    printf("Head data: %d\n", head->data);

    insertFirst(20, &head);
    printf("Head data: %d\n", head->data);

    insertFirst(30, &head);
    printf("Head data: %d\n", head->data);

    insertFirst(40, &head);
    printf("Head data: %d\n", head->data);

    printAll(head);

    return 0;
}

void insertFirst(int data, node** head) {
    *head = crearNodo(data, *head);
}

node* crearNodo(int data, node* next) {
    node* newNode = (node*) malloc(sizeof(node));
    if(newNode == NULL) {
        printf("Error al crear nuevo nodo\n");
        exit(0);
    }
    newNode->data = data;
    newNode->next = next;

    return newNode;
}

void printAll(node* head) {
    node* current = head;
    int i = 1;
    while(current != NULL) {
        printf("Node %d data: %d\n", i++, current->data);
        current = current->next;
    }
}
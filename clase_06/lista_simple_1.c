#include<stdio.h>
#include<stdlib.h>

int main() {
    struct node {
        int data;
        struct node *next;
    };

    struct node *head;
    head = (struct node*) malloc(sizeof(struct node));
    head->next = NULL;
    head->data = 5;

    printf("Head data: %d\n", head->data);

    free(head);

    return 0;
}
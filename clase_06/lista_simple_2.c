#include<stdio.h>
#include<stdlib.h>

typedef struct node {
    int data;
    struct node *next;
} node;

node* crearNodo(int, node*);

int main() {
    node* head = NULL;
    head = crearNodo(10, NULL);

    printf("Head data: %d\n", head->data);
    return 0;
}

node* crearNodo(int data, node* next) {
    node* newNode = (node*) malloc(sizeof(node));
    if(newNode == NULL) {
        printf("Error al crear nuevo nodo\n");
        exit(0);
    }
    newNode->data = data;
    newNode->next = next;

    return newNode;
}
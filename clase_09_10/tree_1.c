#include <stdio.h>
#include <stdlib.h>
#include "./tree_1.h"

struct node *root = NULL;

int main() {
    insertNode(50, &root);
    insertNode(30, &root);
    insertNode(20, &root);
    insertNode(40, &root);
    insertNode(70, &root);
    insertNode(60, &root);
    insertNode(80, &root);

    printInOrder(root);

    destroyTree(root);

    return 0;
}

void destroyTree(struct node* leaf) {
    if(leaf != NULL) {
        destroyTree(leaf->left);
        destroyTree(leaf->right);
        free(leaf);
    }
    return;
}

struct node* createNewNode(int data) {
    struct node *temp;
    temp = (struct node*) malloc(sizeof(struct node));
    temp->clave = data;

    // Debemos inicializar los hijos con NULL
    temp->left = NULL;
    temp->right = NULL;

    return temp;
}

void insertNode(int data, struct node** leaf) {
    if(*leaf == NULL) { // Si no existe sub-árbol
        *leaf = createNewNode(data);
    } else if(data <= (*leaf)->clave) {
        insertNode(data, &((*leaf)->left));
    } else { // Si data > (*leaf)->clave
        insertNode(data, &((*leaf)->right));
    }
    return;
}

struct node* searchRecursiveNode(int data, struct node* leaf) {
    if(leaf != NULL) {
        if(data == leaf->clave) {
            return leaf;
        } else if(data <= leaf->clave) {
            return searchRecursiveNode(data, leaf->left);
        } else {
            return searchRecursiveNode(data, leaf->right);
        }
    } else {
        return NULL;
    }
}

struct node* searchIterativeNode(int data, struct node* leaf) {
    struct node* temp = leaf;
    while (temp != NULL && data != temp->clave) {
        if(data <= temp->clave) {
            temp = temp->left;
        } else {
            temp = temp->right;
        }
    }
    return temp;
}

struct node* searchMinValue(struct node* leaf) {
    if(leaf == NULL) {
        return NULL;
    } else if(leaf->left == NULL) {
        return leaf; // Llegamos al mínimo
    } else { // Seguimos buscando hacia la izquierda
        return searchMinValue(leaf->left);
    }
}

struct node* searchMaxValue(struct node* leaf) {
    if(leaf == NULL) {
        return NULL;
    } else if(leaf->right == NULL) {
        return leaf; // Llegamos al máximo
    } else { // Seguimos buscando hacia la derecha
        return searchMaxValue(leaf->right);
    }
}

void printInOrder(struct node *leaf) {
    if(leaf != NULL) {
        printInOrder(leaf->left);
        printf("%d -> ", leaf->clave);
        printInOrder(leaf->right);
    }
    return;
}

void printPreOrder(struct node *leaf) {
    if(leaf != NULL) {
        printf("%d -> ", leaf->clave);
        printInOrder(leaf->left);
        printInOrder(leaf->right);
    }
    return;
}

void printPostOrder(struct node *leaf) {
    if(leaf != NULL) {
        printInOrder(leaf->left);
        printInOrder(leaf->right);
        printf("%d -> ", leaf->clave);
    }
    return;
}
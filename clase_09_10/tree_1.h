struct node {
    int clave; // data
    struct node* left;
    struct node* right;
};

void destroyTree(struct node*);
void insertNode(int, struct node**);
void printInOrder(struct node*);
void printPreOrder(struct node*);
void printPostOrder(struct node*);
struct node* createNewNode(int);
struct node* searchRecursiveNode(int, struct node*);
struct node* searchIterativeNode(int, struct node*);
struct node* searchMinValue(struct node*);
struct node* searchMaxValue(struct node*);
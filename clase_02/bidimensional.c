#include<stdio.h>

int main() {
    int arreglo[4][2] = { {1,2}, {5,7}, {9,3}, {6,8} };

    // 0, 1, 2, 3
    // arreglo[0][0] = 1
    // arreglo[1][0] = 5
    // arreglo[0][1] = 2

    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 2; j++) {
            printf("arreglo[%d][%d] = %d\n", i, j, arreglo[i][j]);
        }
    }

    return 0;
}
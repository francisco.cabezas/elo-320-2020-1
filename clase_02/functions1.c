#include<stdio.h>

//Declarar función
int multiplica(int a, int b);

int main() {
    int valor1, valor2;

    printf("Ingrese valor 1: ");
    scanf("%d", &valor1);

    printf("Ingrese valor 2: ");
    scanf("%d", &valor2);

    int resultado = multiplica(valor1, valor2);

    printf("Resultado de la multiplicación: %d\n", resultado);
    //printf("Valor de c fuera de la función: %d\n", c);

    return 0;
}

//Implementación de la función
int multiplica(int a, int b) {
    return a * b;
}
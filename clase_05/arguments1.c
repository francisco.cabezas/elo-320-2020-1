#include<stdio.h>
#include<string.h>

int main(int argc, char* argv[]) {
    if(argc == 2) { //Solo se pasó 1 parámetro
        printf("Argumento ingresado es %s\n", argv[1]);
    } else if(argc > 2) {
        for(int i = 0; i < argc; i++) {
            printf("El argumento en la posición %d es %s\n", i, argv[i]);
        }
    } else {
        printf("No se ingresó ningún argumento adicional\n");
    }
    return 0;
}
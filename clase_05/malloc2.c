#include<stdio.h>
#include<stdlib.h>

int main() {
    int largo, n;
    char *buffer;

    printf("Ingrese largo del string aleatorio: ");
    scanf("%d", &largo);

    buffer = (char*) malloc((largo + 1) * sizeof(char));
    if(buffer == NULL) return -1;

    for(n = 0; n < largo; n++) {
        buffer[n] = rand()%26 + 'a';
    }
    buffer[largo] = '\0';

    printf("String aleatorio: %s\n", buffer);
    free(buffer);

    return 0;
}
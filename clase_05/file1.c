#include<stdio.h>
#include<stdlib.h>

int main() {
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    FILE *pfile = fopen("./file.txt", "r");
    if(pfile == NULL){
        return -1;
    }

    while ((read = getline(&line, &len, pfile)) != -1) {
        printf("La linea tiene largo: %zu:\n", read);
        printf("%s", line);
    }

    fclose(pfile);
    if (line) {
        free(line);
    }
    return 0;
}
#include <stdio.h>
#include <stdlib.h>

int main ( ) {
    int size;
    printf("Ingrese tamaño de array: ");
    scanf("%d", &size);

    int *p = (int*) malloc(sizeof(int) * size);
    
    for(int i = 0; i < size; i++) {
        printf("Ingrese valor del indice %d: ", i);
        scanf("%d", &p[i]);
    }
    
    /* Valor de cada elemento del puntero p */ 
    for (int i = 0; i < size; i++ ) {
        printf("*(p + %d) : %d\n", i, *(p + i) );
    }

    free(p);

    return 0;
}
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main() {
    char nombre[20], apellido[20], *info = NULL;

    strcpy(nombre, "Francisco");
    strcpy(apellido, "Cabezas");

    info = (char*) malloc(30 * sizeof(char));
    if(info != NULL) {
        strcpy(info, "es el profesor de EDA ");
    }

    info = realloc(info, 80 * sizeof(char));
    if(info != NULL) {
        strcat(info, "y es Telemático :)");
    } else {
        return -1;
    }

    printf("Nombre = %s, Apellido = %s\n", nombre, apellido);
    printf("Info: %s\n", info);

    free(info);

    return 0;
}
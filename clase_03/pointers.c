#include<stdio.h>

int main() {
    int var = 20;
    int *ip;

    ip = &var;

    printf("Dirección de memoria de var: %x\n", &var);
    printf("Dirección guardada en ip: %x\n", ip);

    printf("Valor al que apunta ip: %d\n", *ip);

    return 0;
}
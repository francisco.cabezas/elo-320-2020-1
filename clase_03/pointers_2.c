#include <stdio.h>
#include <stdlib.h>

int main ( ) {
    double arr [5] = {10.0, 2.0, 3.4, 17.0, 50.0}; 

    int size;
    printf("Ingrese tamaño de array: ");
    scanf("%d", &size);

    int *p = (int*) malloc(sizeof(int) * size);
    
    for(int i = 0; i < size; i++) {
        printf("Ingrese valor del indice %d: ", i);
        scanf("%d", &p[i]);
    }
    
    /* Valor de cada elemento del puntero p */ 
    for (int i = 0; i < size; i++ ) {
        printf("*(p + %d) : %d\n", i, *(p + i) );
    }

    printf("Valores del array arr\n");
    for (int i = 0; i < 5; i++ ) {
        printf("*(arr + %d) : %f\n", i, *(arr + i) );
    }

    return 0;
}
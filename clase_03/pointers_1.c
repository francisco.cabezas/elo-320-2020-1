#include <stdio.h>

int main () {
    int V; 
    int *pt1; 
    int **pt2;

    V = 3000;

    pt1 = &V; /* Toma la dirección de V */
    pt2 = &pt1; /* Toma la dirección de pt1 */

    printf ("Valor en V = %d\n", V); 
    printf ("Dirección en V = %x\n", &V);       // pt1 -> dirección de memoria de V (&V)
                                                // pt2 -> dirección de memoria de pt1 (&pt1)
    printf ("\n"); 
                                                // *pt1 -> valor de la variable a la que apunta pt1 (V)
    printf ("Valor en *pt1 = %d\n", *pt1);      // *pt2 -> valor de pt1 (pt1)
    printf ("Dirección en *pt1 = %x\n", &pt1);  // **pt2 -> Valor de V (valor de la variable a la que apunta pt1)
    printf ("Dirección a la que apunta *pt1 = %x\n", pt1); 

    printf ("\n"); 

    printf ("Valor en **pt2 = %d\n", **pt2);
    printf ("Valor en *pt2 = %x\n", *pt2);
    printf ("Dirección en **pt2 = %x\n", &pt2);
    printf ("Dirección a la que apunta **pt2 = %x\n", pt2); 

    return 0;
}